package item73;

/**
 * 第73条：避免使用线程组
 */
public class Main01 {

    public static void main(String[] args) {
        // 因为线程组已经过时了，所以实际上根本没有必要修正。
    }

}
