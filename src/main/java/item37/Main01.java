package item37;

/**
 * 第37条：用标记接口定义类型
 */
public class Main01 {

    public static void main(String[] args) {
        /**
         * @see java.io.Serializable
         * 通过实现这个接口，类表明它的实例可以被写到ObjectOutputStream(被序列化)
         * 标记接口：没有包含方法声明的接口，而只是指明(标明)一个类实现了具有某种属性的接口。
         */

        // 标记接口定义的类型是由被标记类的实例实现的;
        // 标记注解则没有定义这样的类型。
        // 标记接口在编译时就知道，标记注解在运行时才知道

        // 如果你发现自己在编写的是目标为ElementType.TYPE的标记注解类型，就要花点时间考虑清楚，它是否直的应该为注解类型，
        // 想想标记接口是否会更加合适呢。
    }

}
