package item67;

import java.util.HashSet;

/**
 * 第67条：避免过度同步
 */
public class Main02 {

    public static void main(String[] args) {
        // 为了避免活性失败和安全性失败，在一个被同步的方法或者代码块中，永远不要放弃对客户端的控制。
        ObservableSet<Integer> set = new ObservableSet<Integer>(new HashSet<Integer>());
        set.addObserver(new SetObserver<Integer>() {
            @Override
            public void added(ObservableSet<Integer> set, Integer element) {
                System.out.println(element);
                if (element == 23) set.removeObserver(this);
            }
        });
        for (int i = 0; i < 100; i++)
            set.add(i);
    }

}
