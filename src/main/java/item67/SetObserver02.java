package item67;

public interface SetObserver02<E> {
    // Invoked when an element is added to the observable set
    // 将元素添加到observable中集时调用
    void added(ObservableSet02<E> set, E element);
}
