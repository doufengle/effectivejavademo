package item72;

/**
 * 第72条:不要依赖于线程调度器
 */
public class Main01 {

    public static void main(String[] args) {

        // 任何依赖于线程调度器来达到正确性或者性能要求的程序，很有可能都是不可移植的。

        // 如果线程没有在做有意义的工作，就不应该运行。

        // 不要企图通过调用Thread.yield来"修正"该程序。

        // Thread.yield没有可测试的语义。

        // 线程优先级是JAVA平台上最不可移植的特征了。

        // Thread.yield的唯一用途是在测试期间人为地增加程序的并发性。
    }

}
