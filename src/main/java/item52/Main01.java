package item52;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * 第52条：通过接口引用对象
 */
public class Main01 {

    public static void main(String[] args) {

        // 如果有合适的接口类型存在，那么对于参数、返回值、变量和域来说，就都应该使用接口类型进行声明。


        // good - uses interface as type
        // 好的 - 用接口作为引用对象
        List<Integer> integerList = new Vector<>();

        // bad - uses class as type!
        // 坏的 - 用类作为引用对象
        Vector<Integer> IntegerVector = new Vector<>();

        // 如果你养成了用接口作为类型的习惯，你的程序将会更加灵活。

        // 如果想把实现vector改为arrayList只需要更改后面就行了
        List<Integer> subscribers = new ArrayList<>();

        // 如果没有合适的接口在，完全可以用类而不是接口来引用对象。

        // 不存在适当接口类型第一种情形：
        // 例如String和BigInteger
        // Random

        // 不存在适当接口类型第二种情形：
        // 对象属于一个框架，框架基本类型是类，不是接口。
        // 对象属于这种基于类的框架，就应该用相关的基类(往往是抽象类)来引用这个对象，而不是用它的实现类。
        // java.util.TimerTask抽象类就属于这种情形。

        // 不存在适当接口类型第三种情形：
        // 类实现了接口，但是它提供了接口中不存在的额外方法--例如LinkedHashMap.
        // 如果程序依赖于这些额外的方法，这种类就应该只被用来引用它的实例。它很少应该被用作参数类型。

    }

}
