package item50;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 第50条：如果其他类型更适合，则尽量避免使用字符串
 */
public class Main01 {

    public static void main(String[] args) {
        // 字符串不适合代替其他的值类型。 (该是啥就是啥)
        // 字符串不适合代替枚举类型。
        // 字符串不适合代替聚集类型。
        String className = "Main01";
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        for (Iterator<Integer> i = list.iterator(); i.hasNext();) {
            // inappropriate use of string as aggregate type
            // 字符串不适合代替聚集类型。
            String compoundKey = className + "#" + i.next();
            System.out.println(compoundKey);
        }

        /**
         * @see ThreadLocal01
         * 字符串也不适合代替能力表(capabilities)
         * 1.客户端提供的字符串键必须是唯一的：
         * 2.如果两个客户端各自决定为它们的线程局部变量使用同样的名称
         * 无意中共享了这个变量，两个客户端都失败。而且，安全性也很差。
         */



    }

}
