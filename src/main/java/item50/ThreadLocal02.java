package item50;

public class ThreadLocal02 {

    private ThreadLocal02() {} // Noninstantiable

    public static class Key {  // (capability) 能力
        Key() {}
    }

    // generates a unique, unforgeable key

    public static Key getKey() {
        return new Key();
    }

    public static void set(Key key, Object value){

    }

    public static Object get(Key key){
        return null;
    }

}
