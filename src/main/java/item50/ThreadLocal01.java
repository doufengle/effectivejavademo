package item50;

// broken - inappropriate use of string as capability!
// 破损的 - 不恰当的使用String的能力
public class ThreadLocal01 {

    private ThreadLocal01() {} // Noninstantiable

    // Sets the current thread's value for the named variable.
    // 设置当前线程的值对于名称变量
    public static void set(String key, Object value){

    }

    // returns the current thread's value for the named variable.
    // 返回当前线程的值对于命名变量
    public static Object get(String key){
        return null;
    }

}
