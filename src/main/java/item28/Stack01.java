package item28;

import java.util.Arrays;
import java.util.Collection;
import java.util.EmptyStackException;
import java.util.Iterator;


public class Stack01<E> {

    private E[] elements;
    private int size = 0;
    private static final int DEFAULT_INITIAL_CAPACITY = 16;

    // the elements array will contain only E instances from push(E).
    // this is sufficient to ensure type safety, but the runtime
    // type of the array won't be E[]; it will always be Object[]!
    @SuppressWarnings("unchecked")
    public Stack01() {
        elements = (E[])new Object[DEFAULT_INITIAL_CAPACITY];
    }

    public void push(E e) {
        ensureCapacity();
        elements[size++] = e;
    }

    public E pop() {
        if (size == 0)
            throw new EmptyStackException();
        E result = elements[--size];
        elements[size] = null; //eliminate obsolete reference 消除过时的引用
        return result;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    private void ensureCapacity() {
        if (elements.length == size)
            elements = Arrays.copyOf(elements, 2 * size + 1);
    }

    // pushAll method without wildcard type - deficient
    // pushAll 方法没有通配符类型-缺陷
    public void pushAll(Iterable<E> src) {
        for (E e : src)
            push(e);
    }

    // popAll method without wildcard type - deficient!
    // popAll 方法没有通配符类型-缺陷
    public void popAll(Collection<E> dst) {
        while (!isEmpty())
            dst.add(pop());
    }

}
