package item56;

/**
 * 第56条：遵守普遍接受的命名惯例
 */
public class Main01 {

    public static void main(String[] args) {
        // the java language specification

        // 包层次状 例如edu.cmu、com.sun、gov.nsa

        // 包名称 比较简短,通常不超过8个字符，鼓励使用有意义的缩写形式，例如，使用util而不是utilities.
        // 只取首字母的缩写形式也是可以接受的，例如awt.

        // 类名都应该包括一个或者多个单词，每个单词的首字母大写，例如Timer和TimerTask.
        // 应该尽量避免用缩写，除非是一些首字母缩写和一些通用的缩写，比如max和min.
        // 对于首字母编写，到底应该全部大写还是只有首字母大写，没有统一的说法。
        // 虽然大写更常见一些，但还是强烈建议采用仅有首字母大写的形式：
        // 即使连续出现多个首字母缩写的形式，你仍然可以区分出一个单词的起始处和结束处。
        // 例如:HTTPURL还是HttpUrl?还是HttpUrl更好看一些

        // 方法名称 域名称： 遵守类的命名，首字母小写，例如：remove、ensureCapacity.
        // 如果由首字母编写组成的单词是一个方法或者域名称的第一个单词，它就应该是小写形式。

        // 常量域：它的名称应该包含一个或者多个大写的单词，中间用下划线符号隔开，例如VALUES或NEGATIVE_INFINITY.
        // 常量域是个静态final域，它的值是不可变的。
        // 如果静态final域有基本类型，或者有不可变的引用类型，它就是个常量域。
        // 例如，枚举常量是常量域。
        // 如果静态final域有个可变的引用类型，若被引用的对象是不可变的，它也仍然可以是个常量域。
        // 常量域是唯一推荐使用下划线的情形。

        // 局部变量名称的字面命名惯例与成员名称类似，
        // 只不过它也允许缩写，单个字符和短字符序列的意义取决于局部变量所在的上下文环境，例如i、xref和houseNumber

        // 类型参数名称通常由单个字母组。这个字母通常是以下五种类型之一：
        // T表示任意的类型，E表示集合的元素类型，K和V表未映射的键和值类型，X表示异常。
        // 任何类型的序列可以是T、U、V或者T1、T2、T3.


        // 字面惯例的例子：
        // 包:com.google.inject, org.joda.time.format
        // 类或者接口:Timer, FutureTask, LinkedHashMap, HttpServlet
        // 方法或者域:remove, ensureCapacity, getCrc
        // 常量域:MIN_VALUE, NEGATIVE_INFINITY
        // 局部变量:i, xref, houseNumber
        // 类型参数:T,E,K,V,X,T1,T2


        // 语法命名惯例：
        // 类通常用一个名词或者名词短语命名，例如Timer、BufferedWriter或者ChessPiece.

        // 接口的命名与类相似，例如Collection或Comparator,
        // 或者用一个以“-able”或“-ible”结尾的形容词来命名，例如Runnable、Iterable或者Accessible.

        // 由于注解类型有这么多用处，因此没有单独安排词类。
        // 名词、动词、介词和形容词都很常用，例如BindingAnnotion、Inject、ImplementedBy或者Singleton.

        // 执行某个动作的方法通常用动词或者动词短语来命名，例如append或drawImage.
        // 对于返回boolean值的方法，其名称往往以单词“is”开头，很少用has,后面跟名词或名词短语
        // 或者任何具有形容词功能的单词或短语，例如isDigit、isProbablePrime、isEmpty、isEnabled或者hasSiblings.

        // 如果方法返回被调用对象的一个非boolean的函数或者属性，它通常用名词、名词短语或者以动词"get"开头的动词短语来命名
        // 例如size、hashCode或者getTime.
        // 有一种声音认为，只有第三种形式(以“get”开头)才可以接受，但是这种说法没有什么根据。
        // 普通类采用size、hashCode
        // 如果方法所在的类是个Bean，就要强制使用以“get”开头的形式，
        // 而且，如果考虑将来要把这个类转变成Bean,这么做也是明智的。
        // 另外，如果这个类包含一个方法用于设置同样的属性，则强烈建议采用这种形式。
        // 在这种情况下，这两个方法应该被命名为getAttribute和setAttribute.

        // 方法：转换对象类型的方法、返回不同类型的独立对象的方法，通常被称为toType,
        // 例如toString和toArray.
        // 返回视图(view,视图类型不同于接收对象的类型)的方法通常被称为asType,例如asList.
        // 返回一个与被调用对象同值的基本类型的方法，通常被称为typeValue,例如intValue.
        // 静态工厂的常用名称为valueOf、of、getInstance、newInstance、getType和NewType.

        // 域名称的语法惯例：
        // boolean类型的域命名与boolean类型的访问方法(accessor method)很类似，但是省去了初始的"is"，
        // 例如initialized和composite.
        // 其他类型的域通常用名词或者名词短语来命名，比如height、digits或bodyStyle.
        // 局部变量的语法惯例类似于域的语法惯例，但是更弱一些。

    }

}
