package item74;

/**
 * 第74条：谨慎地实现Serializable接口
 */
public class Main01 {

    public static void main(String[] args) {
        // 实现serializable接口而付出的最大代价是，一旦一个类被发布，就大大降低了“改变这个类的实现”的灵活性。

        // 实现Serializable的第二个代价是，它增加了出现bug和安全漏洞的可能性。

        // 实现Serializable的第三个代价是，随着类发行新的版本，相关的测试负担也增加了。

        // 实现Serializable接口并不是一个很轻松就可以做出的决定。

        // Date和BigInteger这样的值类应该实现Serializable，大多数的集合类也应该如此。
        // 代表活动实体的类，比如线程池(thread pool),一般不应该实现Serializable。

        // 为了继承而设计的类应该尽可能少地去实现Serializable接口，用户的接口也应该尽可能少地继承Serializable接口。

        // 框架要求所有的参与者都必须实现Serializable接口，那么对于这个类或者接口来说，实现或者扩展Serializable接口就是非常有意义的。
        // 为了继承而设计的类中，真正实现了Serializable接口的有Throwable类、component和HttpServlet抽象类。

        // 对于为继承而设计的不可序列化的类，你应该考虑提供一个无参构造器。

        // 内部类不应该实现Serializable.

        // 内部类的默认序列化形式是定义不清楚的。


    }

}
