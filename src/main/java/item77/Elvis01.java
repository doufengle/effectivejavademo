package item77;

import java.io.Serializable;

public class Elvis01 implements Serializable {

    public static final Elvis01 INSTANCE = new Elvis01();

    private Elvis01() {
        // ...
    }

    public void leaveTheBuilding() {
        // ...
    }

    // readResolve for instance control - you can do better!
    private Object readResolve() {
        // return the one true Elvis and let the garbage collector
        // take care of the Elvis impersonator.
        return INSTANCE;
    }

}
