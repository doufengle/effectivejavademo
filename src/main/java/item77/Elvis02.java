package item77;

import java.util.Arrays;

public enum  Elvis02 {

    INSTANCE;

    private String[] favoriteSongs = {"Hound Dog", "Heartbreak hotel"};

    public void printFavorites() {
        System.out.println(Arrays.toString(favoriteSongs));
    }

}
