package item77;

/**
 * 第77条: 对于实例控制，枚举类型优先于readResolve
 */
public class Main01 {

    public static void main(String[] args) {

        // 如果依赖readResolve进行实例控制，带有对象引用类型的所有实例域则都必须声明为transient的。

        // readResolve的可访问性很重要。
    }

}
