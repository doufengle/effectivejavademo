package item44;

/**
 * an instrument section of a symphony orchestra
 * 交响乐团的乐器部分
 */
public enum OrchestraSection {

    /** Woodwinds, such as flute, clarinet, and oboe. */
    /** 木管乐器，如长笛，单簧管和双簧管。*/
    WOODWIND,

    /** Brass instruments, such as french horn and trumpet. */
    /** 黄铜乐器，如法国号和小号。 */
    BRASS,

    /** Percussion instruments, such as timpani and cymbals */
    /** 打击乐器，如定音鼓和钹 */
    PERCUSSION,

    /** Stringed instruments, such as violin and cello. */
    /** 弦乐器，如小提琴和大提琴 */
    STRING;

}
