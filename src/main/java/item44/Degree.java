package item44;

/**
 * A college degree, such as B.S.,{@literal M.S.} or Ph.D.
 * College is fountain of knowledge where many go to drink.
 *
 * 大学学位，例如 学士、硕士、博士
 * 大学是很多人获取知识源泉的地方。
 */
public class Degree {

    // ...

}
