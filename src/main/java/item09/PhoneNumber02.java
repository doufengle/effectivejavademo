package item09;

public class PhoneNumber02 {

    private final int areaCode;
    private final int prefix;
    private final int lineNumber;

    public PhoneNumber02(int areaCode, int prefix, int lineNumber) {
        this.areaCode = areaCode;
        this.prefix = prefix;
        this.lineNumber = lineNumber;
    }

    private static void rangeCheck(int arg, int max, String name) {
        if (arg < 0 || arg > max)
            throw new IllegalArgumentException(name + ": " + arg);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneNumber02 that = (PhoneNumber02) o;
        return areaCode == that.areaCode &&
                prefix == that.prefix &&
                lineNumber == that.lineNumber;
    }

    // the worst possible legal hash function - never use!
    @Override
    public int hashCode() {
        return 42;
    }

    // remainder omitted

}
