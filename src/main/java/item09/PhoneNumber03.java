package item09;

import java.util.Objects;

public class PhoneNumber03 {

    // lazily initialized, cached hashCode
    private volatile int hashCode; // see item77 71

    private final int areaCode;
    private final int prefix;
    private final int lineNumber;

    public PhoneNumber03(int areaCode, int prefix, int lineNumber) {
        this.areaCode = areaCode;
        this.prefix = prefix;
        this.lineNumber = lineNumber;
    }

    private static void rangeCheck(int arg, int max, String name) {
        if (arg < 0 || arg > max)
            throw new IllegalArgumentException(name + ": " + arg);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneNumber03 that = (PhoneNumber03) o;
        return areaCode == that.areaCode &&
                prefix == that.prefix &&
                lineNumber == that.lineNumber;
    }

    @Override
    public int hashCode() {
        int result = hashCode;
        if (result == 0) {
            result = Objects.hash(areaCode, prefix, lineNumber);
        }
        return result;
    }

    // remainder omitted

}
