package item09;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Test01 {

    public static void main(String[] args) {
        // equals方法的比较操作所用到的信息没有被修改，对于这个对象调用多次，hashCode方法都必须始终如一地返回同一个整数
        // 同一个应用程序的多次执行，每次执行所返回的整数可以不一致
        // 如果两个对象根据equals方法比较相等，那么hashCode产生同样的整数结果
        // 如果两个对象equals不相等，hashCode不一定要产生不同的整数结果，但产生不同的结果，可以提高散列表的性能

        // 在覆盖了equals方法的类中，没有覆盖hashCode方法，导致该类无法结合所有基于散列的集合一起正常运作(HashMap、hashSet、hashtable)
        // 因没有覆盖hashCode而违反的关键约定是第二条;相等的对象必须具有相等的散列码
        Map<PhoneNumber01, String> m = new HashMap<PhoneNumber01, String>();
        PhoneNumber01 putObject = new PhoneNumber01(707, 867, 5309);
        PhoneNumber01 getObject = new PhoneNumber01(707, 867, 5309);
        System.out.println(putObject.equals(getObject));
        m.put(putObject, "Jenny");
        System.out.println(m.get(getObject));

        // 使用一个不恰当的hashCode方法
        Map<PhoneNumber02, String> m2 = new HashMap<>();
        PhoneNumber02 putObject02 = new PhoneNumber02(707, 867, 5309);
        PhoneNumber02 getObject02 = new PhoneNumber02(707, 867, 5309);
        System.out.println(putObject02.equals(getObject02));
        m2.put(putObject02, "Jenny");
        System.out.println(m2.get(getObject02));

        // 使用延迟初始化的hashCode


    }

}
