package item30;

// enum type that switches on its own value - questionable
// 枚举类型用switch展现他的价值---值得考虑
public enum Operation01 {
    PLUS, MINUS, TIMES, DIVIDE;

    // Do the arithmetic op represented by this constant
    // 由此常量表示运算符
    double apply(double x, double y) {
        switch (this) {
            case PLUS: return x + y;
            case MINUS: return x - y;
            case TIMES: return x * y;
            case DIVIDE: return x / y;
        }
        throw new AssertionError("Unknown op: " + this);
    }
}
