package item30;

/**
 * 第30条：用enum代替int常量
 */
public class Main01 {

    public static void main(String[] args) {

        /**
         * @see Constant
         * int 枚举模式
         */
        // Tasty citrus flavored applesauce!
        int i = (Constant.APPLE_FUJI - Constant.ORANGE_TEMPLE) / Constant.APPLE_PIPPIN;
        System.out.println(i);

        /**
         * @see Apple
         * @see Orange
         * 枚举
         */

        /**
         * @see Planet
         * 为了将数据与枚举常量关联起来，得声明实例域，并编写一个带有数据并将数据保存在域中的构造器。
         */

        double x = 2;
        double y = 4;
        for (Operation03 op : Operation03.values())
            System.out.printf("%f %s %f = %f%n", x, op, y, op.apply(x, y));




    }

    // Switch on an enum to simulate a missing method
    // 返回每个运算的反运算
    public static Operation03 inverse(Operation03 op) {
        switch (op) {
            case PLUS: return Operation03.MINUS;
            case MINUS: return Operation03.PLUS;
            case TIMES: return Operation03.DIVIDE;
            case DIVIDE: return Operation03.TIMES;
            default: throw new AssertionError("Unknown op: " + op);
        }
    }

}
