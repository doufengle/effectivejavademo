package item58;

/**
 * 第58条：为可恢复的情况使用受检异常，对编程错误使用运行时异常
 */
public class Main01 {

    public static void main(String[] args) {
        // java程序设计语言提供3种可抛出结构：
        // 1.受检的异常(checked exception)
        // 2.运行时异常(run-time exception)
        // 3.错误(error)

        // 如果期望调用者能够适当地恢复，对于这种情况就应该使用受检的异常。

        // 用运行时异常来表明编程错误。

        // 你实现的所有未受检的抛出结构都应该是RuntimeException的子类


    }

}
