package item66;

import java.util.concurrent.TimeUnit;

// broken! - how long would you expect this program to run?
// 破损的 - 你期望这个程序运行多行时间？
public class StopThread {

    private static  boolean stopRequested;

    public static void main(String[] args) throws InterruptedException {
        Thread backgroundThread = new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while (!stopRequested)
                    i++;
            }
        });
        backgroundThread.start();
        TimeUnit.SECONDS.sleep(1);
        stopRequested = true;
    }

}
