package item66;

import java.util.concurrent.TimeUnit;

// cooperative thread termination with a volatile field
// 具有易失性字段的协作线程终止
public class StopThread03 {


    private static volatile  boolean stopRequested;

    public static void main(String[] args) throws InterruptedException {
        Thread backgroundThread = new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while (!stopRequested)
                    i++;
            }
        });
        backgroundThread.start();
        TimeUnit.SECONDS.sleep(1);
        stopRequested = true;
    }

}
