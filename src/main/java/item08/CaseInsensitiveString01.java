package item08;

import java.util.Objects;

// broken - violates symmetry
// 违反对称性约定
public class CaseInsensitiveString01 {

    private final String s;

    public CaseInsensitiveString01(String s) {
        if (s == null)
            throw new NullPointerException();
        this.s = s;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof CaseInsensitiveString01)
            return s.equalsIgnoreCase(((CaseInsensitiveString01) o).s);
        if (o instanceof String)  //one-way interoperability  单向互通性
            return s.equalsIgnoreCase((String)o);
        return false;
    }

    // 去掉单向互通
//    @Override
//    public boolean equals(Object o) {
//        return o instanceof CaseInsensitiveString01 &&
//                ((CaseInsensitiveString01) o).s.equalsIgnoreCase(s);
//    }

    // remainder omitted
}
