package item08;

public class Point03 {

    private final int x;
    private final int y;

    public Point03(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        // ...
        return true;
    }

// remainder omitted

}
