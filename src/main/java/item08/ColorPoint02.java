package item08;

import java.awt.*;

public class ColorPoint02 extends Point01{

    private final Color color;

    public ColorPoint02(int x, int y, Color color) {
        super(x, y);
        this.color = color;
    }

    // broken - violates symmetry! 破坏了对称性
    // point类可以等于ColorPoint01,但是ColorPoint01不等于point
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof ColorPoint02))
            return false;
        return super.equals(o) && ((ColorPoint02)o).color == color;
    }

    // remainder omitted

}
