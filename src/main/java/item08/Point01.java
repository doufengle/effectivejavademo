package item08;

import java.util.Objects;

public class Point01 {

    private final int x;
    private final int y;

    public Point01(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Point01))
            return false;
        Point01 point01 = (Point01)o;
        return point01.x == x && point01.y == y;
    }

    // remainder omitted

}
