package item08;

import java.util.Objects;

public class Point02 {

    private final int x;
    private final int y;

    public Point02(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point02 point02 = (Point02) o;
        return x == point02.x &&
                y == point02.y;
    }

// remainder omitted

}
