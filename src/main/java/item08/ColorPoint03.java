package item08;

import java.awt.*;

public class ColorPoint03 extends Point01{

    private final Color color;

    public ColorPoint03(int x, int y, Color color) {
        super(x, y);
        this.color = color;
    }

    // broken - violates transitivity 违反传递性
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Point01))
            return false;
        // if o is a normal point01, do a color-blind comparison
        if (!(o instanceof ColorPoint03))
            return o.equals(this);
        // o is a colorpoint; do a full comparison
        return super.equals(o) && ((ColorPoint03)o).color == color;
    }

    // remainder omitted

}
