package item08;

import java.awt.*;

public class ColorPoint01 extends Point01{

    private final Color color;

    public ColorPoint01(int x, int y, Color color) {
        super(x, y);
        this.color = color;
    }

    // 1使用继承过来的equals方法，color被忽略

    // remainder omitted

}
