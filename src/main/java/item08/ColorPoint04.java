package item08;

import java.awt.*;

public class ColorPoint04 {

    private final Point02 point;
    private final Color color;

    public ColorPoint04(int x, int y, Color color) {
        point = new Point02(x, y);
        this.color = color;
    }

    /**
     * @return the point-view of this color point.
     */
    public Point02 asPoint() {
        return point;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof ColorPoint04))
            return false;
        ColorPoint04 cp = (ColorPoint04) o;
        return cp.point.equals(point) && cp.color.equals(color);
    }

    // remainder omitted

}
