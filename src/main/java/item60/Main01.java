package item60;

/**
 * 第60条：优先使用标准的异常
 */
public class Main01 {

    public static void main(String[] args) {

        // 重用现有的异常有多方面的好处
        // 1.使你的API更加易于学习和使用，因为它与程序员已经熟悉的习惯用法是一致的。
        // 2.对于用到这些API的程序而言，它们的可读性会更好，因为它们不会出现很多程序员不熟悉的异常。
        // 3.(最不重要的)异常类越少，意味着内存印迹就越小，装载这些类的时间开销也越少。

        // 常用的异常
        // IllegalArgumentException  非null的参数值不正确
        // IllegalStateException 对于方法调用而言，对象状态不合适
        // NullPointerException 在禁止使用null的情况下参数值为null
        // IndexOutOfBoundsException 下标参数值越界
        // ConcurrentModificationException 在禁止并发修改的情况下，检测到对象的并发修改
        // UnsupportedOperationException 对象不支持用户请求的方法



    }

}
