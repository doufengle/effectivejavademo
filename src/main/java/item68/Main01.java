package item68;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 第68条：executor和task优先于线程
 */
public class Main01 {

    public static void main(String[] args) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
            }
        });
        executor.shutdown();
    }

}
