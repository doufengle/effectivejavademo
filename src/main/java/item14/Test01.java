package item14;

import java.awt.*;

/**
 * 第14条 在公有类中使用访问方法而非公有域
 */
public class Test01 {

    public static void main(String[] args) {

        Point01 point01 = new Point01();
        point01.x = 1.0;
        //1. 没有提供封装的功能
        //2. 如果不改变API，就无法改变它的数据表示法，也无法强加任何约束条件
        //3. 当域被访问的时候，无法采取任何辅助的行动
        Point02 point02 = new Point02(1.0, 2.0);
        point02.setX(2.0);
        System.out.println(point02.getX());
        // 如果类可以在它所在的包的外部进行访问，就提供访问方法

        // 如果类是包级私有的，或者是私有的嵌套类，直接暴露它的数据域并没有本质的错误
        // 它只在包内用，这样影响的是包内不会影响到包以外的地方
        Point03 point03 = new Point03();
        point03.x = 1.0;

        Time01 time01 = new Time01(12, 43);
        System.out.println(time01.hour);
        System.out.println(time01.minute);

    }

}
