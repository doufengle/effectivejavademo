package item17;

import java.util.Date;

public class Sub extends Super {

    // Blank final, set by constructor
    // 未初化，在构造方法中设置
    private final Date date;

    public Sub() {
        this.date = new Date();
    }

    // overriding method invoked by superclass constructor
    // 超类构造方法调用的覆盖方法
    @Override
    public void overrideMe() {
        System.out.println(date);
    }

}
