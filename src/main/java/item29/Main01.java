package item29;

/**
 * 第29条：优先考虑类型安全的异构容器
 */
public class Main01 {

    public static void main(String[] args) {

        /**
         * @see Favorites
         * 类型安全的异构容器
         * @see Class#cast(Object)
         * @see java.util.Collections.CheckedSet
         * @see java.util.Collections.CheckedList
         * @see java.util.Collections.CheckedMap
         */

    }

}
