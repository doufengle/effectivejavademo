package item04;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 通过私有构造器强化不可实例化的能力
 */
public class Test01 {

    public static void main(String[] args) {
        //把基本类型的值或者数组类型上的相关方法组织起来
        Math.abs(100);
        int[] a = {0, 1};
        Arrays.sort(a);

        //把实现特定接口的对象上的静态方法组织起来
        List list = Collections.emptyList();

        Integer pageSize = UtilityClass.getPageSize(null);
    }

}
