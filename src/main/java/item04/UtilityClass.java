package item04;

// Noninstantiable utility class
// 非实例化的实用类
public class UtilityClass {

    // suppress default constructor for noninstantiability
    // 抑制默认构造函数用于非实例化
    private UtilityClass() {
        throw new AssertionError();
    }

    /**
     * 一页几条
     */
    public static Integer getPageSize(Integer pageSize) {
        Integer size = 10;
        if (pageSize!= null) {
            size = pageSize;
        }
        return size;
    }

    // Remainder omitted
    // 剩余部分省略
}
