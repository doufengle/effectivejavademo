package item39;

import java.util.Date;

public class Period02 {

    private final Date start;

    private final Date end;

    public Period02(Date start, Date end) {
        this.start = new Date(start.getTime());
        this.end = new Date(end.getTime());
        if (this.start.compareTo(this.end) > 0)
            throw new IllegalArgumentException(this.start + " after " + this.end);
    }

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }

    // ...
    // remainder omitted

}
