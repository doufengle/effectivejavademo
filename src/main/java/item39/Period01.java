package item39;

import java.util.Date;

// broken "immutable" time period class
// 破碎 不可变时间段类
public class Period01 {

    private final Date start;

    private final Date end;

    public Period01(Date start, Date end) {
        if (start.compareTo(end) > 0)
            throw new IllegalArgumentException(start + " after " + end);
        this.start = start;
        this.end = end;
    }

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }

    // ...
    // remainder omitted

}
