package item39;

import java.util.Date;

/**
 * 第39条：必要时进行保护性拷贝
 */
public class Main01 {

    public static void main(String[] args) {

        // 假设类的客户端会尽其所能来破坏这个类的约束条件，因此你必须保护性地设计程序。

        // 对于构造器的每个可变参数进行保护性拷贝(defensive copy)是必要的

        // 保护性拷贝是在检查参数的有效性之前进行的，并且有效性检查是针对拷贝之后的对象，而不是针对原始的对象。

        // 对于参数类型可以被不可信任方子类化的参数，请不要使用clone方法进行保护性拷贝。

        // 使它返回可变内部域的保护性拷贝即可


        // attack the internals of a Period instance
        // 功击Period实例的内部
        Date start = new Date();
        Date end = new Date();
        Period01 p = new Period01(start, end);
        end.setYear(78); // modifies internals of p!  修改p内部

        // Second attack the internals of a Period instance
        // 第二次功击Period实例的内部
        Period02 p2 = new Period02(start, end);
        p2.getEnd().setYear(78); // modifies internals of p!  修改p内部


    }

}
