package item39;

import java.util.Date;

public class Period03 {

    private final Date start;

    private final Date end;

    public Period03(Date start, Date end) {
        this.start = new Date(start.getTime());
        this.end = new Date(end.getTime());
        if (this.start.compareTo(this.end) > 0)
            throw new IllegalArgumentException(this.start + " after " + this.end);
    }

    // repairedf accessors - make defensive copies of internal fields
    // 制作内部字段的防御性脚本
    public Date getStart() {
        return new Date(start.getTime());
    }

    public Date getEnd() {
        return new Date(end.getTime());
    }

    // ...
    // remainder omitted

}
