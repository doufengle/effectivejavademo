package item22;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;

/**
 * 第22条：优于考虑静态成员类
 */
public class Test01 {

    public static void main(String[] args) {
        // 嵌套类是指被定义在另一个类的内部的类。
        // 嵌套类存在的目的应该只是为它的外围类提供服务。
        // 嵌套类有四种：静态成员类(static member class)、非静态成员类(nonstatic member class)、匿名类(nonstatic member class)、
        // 局部类(local class)

        /**
         * @see item21.Host 静态成员类
         * @see HashSet#iterator() 非静态成员类
         */

        //匿名类
        String[] stringArray = new String[]{"1","2"};
        Arrays.sort(stringArray, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.length() - o2.length();
            }
        });
    }



}
