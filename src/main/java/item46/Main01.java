package item46;

import java.util.*;

/**
 * 第46条：for-each循环优先于传统的for循环
 */
public class Main01 {

    enum Suit {CLUB, DIAMOND, HEART, SPADE}
    enum Rank {ACE, DEUCE, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING}

    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();
        list.add(1);
        // no longer the preferred idiom to iterate over a collection!
        for (Iterator i = list.iterator(); i.hasNext();) {
            System.out.println(i.next()); // (no generics before 1.5)
        }

        Integer[] a = {1,2,3};
        // no longer the preferred idiom to iterate over an array!
        for (int i = 0; i < a.length; i++) {
            //dosomething
        }

        // the preferred idiom for iterating over collections and arrays
        for (Integer i : list) {
            //dosomething
        }

        // can you spot the bug?
        Collection<Suit> suits = Arrays.asList(Suit.values());
        Collection<Rank> ranks = Arrays.asList(Rank.values());
        List<Card> deck = new ArrayList<Card>();
        for (Iterator<Suit> i = suits.iterator(); i.hasNext();)
            for (Iterator<Rank> j = ranks.iterator(); j.hasNext();)
                deck.add(new Card(i.next(), j.next()));

        // 三种常见的情况无法使用for-each循环
        // 过滤 删除元素，显式的迭代器，可以调用它的remove方法
        // 转换 替换元素
        // 平行迭代 遍历多个集合，显示控制步长等

    }

}
class Card {
    Card(Main01.Suit suit, Main01.Rank rank) {

    }
}
