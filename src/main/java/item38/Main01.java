package item38;

import java.math.BigInteger;

/**
 * 第38条：检查参数的有效性
 */
public class Main01 {

    public static void main(String[] args) {
        /**
         * @see BigInteger#mod(BigInteger)
         * 参数检查
         */
        long a[] = {1l,2l,3l};
        sort(null,10, 1);
    }

    private static void sort(long a[], int offset, int length) {
        assert a != null;
        assert offset >= 0 && offset <= a.length;
        assert length >= 0 && length <= a.length - offset;
        // do the computation 做计算

    }

}
