package item20;

/**
 * 第20条：类层次优于标签类
 */
public class Test01 {

    public static void main(String[] args) {

        /**
         * @see Figure
         * 充斥着样板代码，包括枚举声明、标签域以及条件语句。
         * 由于多个实现乱七八糟地挤在了单个类中，破坏了可读性。
         * 内存占用增加了，因为实例承担着属于其他风格的不相关的域。
         * 域不能做成是final的，除非构造器初始化了不相关的域，产生更多的样板代码。
         * 程序会在运行时失败
         *
         * 标签类过于冗长、容易出错，并且效率低下。
         */
        // 标签类过于冗长、容易出错，产并且效率低下。
        // 标签类正是类层次的一种简单的仿效。

        /**
         * @see Figure01
         * @see Circle
         * @see Rectangle
         *
         * 类层次纠正了前面提到过的标签类的所有缺点。
         *
         */

    }

    // Class hierarchy replacement for a tagged class
    // 用类层次替换标签类
    abstract class Figure01 {
        abstract double area();
    }

    class Circle extends Figure01 {

        final double radius;

        public Circle(double radius) {
            this.radius = radius;
        }

        @Override
        double area() {
            return Math.PI * (radius * radius);
        }
    }

    class Rectangle extends Figure01 {

        final double length;
        final double width;

        public Rectangle(double length, double width) {
            this.length = length;
            this.width = width;
        }

        @Override
        double area() {
            return length * width;
        }
    }

    class Square extends Rectangle {
        Square(double side) {
            super(side, side);
        }
    }

}
