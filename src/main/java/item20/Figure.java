package item20;

// tagged class - vastly inferior to a class hierarchy!
// 标签类-远远低于类层次结构
public class Figure {

    enum Shape {RECTANGLE, CIRCLE};

    // Tag field - the shape of this figure
    // 标签属性 - 此图的形状
    final Shape shape;

    // these fields are used only if shape is rectangle
    // 仅当矩形时才使用这些字段
    double length;
    double width;

    // this field is used only if shape is circle
    // 仅当圆形时才使用这些字段(半径)
    double radius;

    // Constructor for circle
    // 对于圆形的构造函数
    public Figure(double radius) {
        shape = Shape.CIRCLE;
        this.radius = radius;
    }

    public Figure(double length, double width) {
        shape = Shape.RECTANGLE;
        this.length = length;
        this.width = width;
    }

    double area() {
        switch (shape) {
            case RECTANGLE:
                return length * width;
            case CIRCLE:
                return Math.PI * (radius * radius);
            default:
                throw new AssertionError();
        }
    }

}
