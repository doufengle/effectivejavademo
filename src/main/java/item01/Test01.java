package item01;

import java.math.BigInteger;
import java.util.Random;

public class Test01 {

    public static void main(String[] args) {
        //考虑用静态工厂方法代替构造器
        Boolean b = Boolean.valueOf(true);

        BigInteger bigInteger = new BigInteger(100,100, new Random());
        BigInteger.probablePrime(100, new Random());

        // service provider framework sketch
        // 服务提供者框架示意图
        // 参见 Service Provider Services
    }

}
