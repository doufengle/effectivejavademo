package item01;

//Service provider interface
public interface Provider {
    Service newService();
}
