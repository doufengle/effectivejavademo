package item01;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

// Noninstantiable class for service registration and access
// 服务注册和访问的非实例化类
public class Services {

    private Services() {}   //Prevents instantiation (Item 4) 防止实例化

    // Maps service names to services
    // 将服务名称映射到服务
    private static final Map<String, Provider> providers = new ConcurrentHashMap<String, Provider>();

    public static final String DEFAULT_PROVIDER_NAME = "<def>";

    // Provider registration API
    // 提供者注册API
    public static void registerDefaultProvider(Provider p) {
        registerDefaultProvider(p);
    }

    public static void registerProvider(String name, Provider p) {
        providers.put(name, p);
    }

    // Service access API
    // 服务访问API
    public static Service newInstance() {
        return newInstance(DEFAULT_PROVIDER_NAME);
    }

    public static Service newInstance(String name) {
        Provider p = providers.get(name);
        if (p == null) {
            throw new IllegalArgumentException("No provider registered with name: " + name);
        }
        return p.newService();
    }


}
