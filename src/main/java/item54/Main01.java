package item54;

/**
 * 第54条: 谨慎使用本地方法
 */
public class Main01 {

    public static void main(String[] args) {
        // java native interface(JNI) 允许java应用程序可以调用本地方法(native method)。
        // 本地方法是指用本地程序设计语言(比如C或者C++)来编写的特殊方法。
        // 本地方法三种用途
        // 1.提供了"访问特定于平台的机制"的能力，比如访问注册表(registry)和文件锁(file lock).
        // 2.提供了访问遗留代码库的能力，从而可以访问遗留数据(legacy data).
        // 3.本地方法可以通过本地语言，编写应用程序中注重性能的部分，以提高系统的性能。

        // 使用本地方法来访问特定于平台的机制是合法的，但是java平台不断成熟，有些功能已经提供
        // 使用本地方法来访问遗留代码也是合法的。

        // 使用本地方法来提高性能的做法不值得提倡。
    }

}
