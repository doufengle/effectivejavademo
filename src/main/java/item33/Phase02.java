package item33;

import java.util.EnumMap;
import java.util.Map;

// using a nested EnumMap to associate data with enum pairs
// 使用嵌套的enumMap将数据和枚举关联起来
public enum Phase02 {

    SOLID, LIQUID, GAS;

    public enum Transition {
        MELT(SOLID, LIQUID),
        FREEZE(LIQUID, SOLID),
        BOIL(LIQUID, GAS),
        CONDENSE(GAS, LIQUID),
        SUBLIME(SOLID, GAS),
        DEPOSIT(GAS, SOLID);

        private final Phase02 src;
        private final Phase02 dst;

        Transition(Phase02 src, Phase02 dst) {
            this.src = src;
            this.dst = dst;
        }

        // initialize the phase transition map
        private static final Map<Phase02, Map<Phase02, Transition>> m =
                new EnumMap<Phase02, Map<Phase02, Transition>>(Phase02.class);

        static {
            for (Phase02 p : Phase02.values())
                m.put(p, new EnumMap<Phase02, Transition>(Phase02.class));
            for (Transition trans : Transition.values())
                m.get(trans.src).put(trans.dst, trans);
        }

        public static Transition from(Phase02 src, Phase02 dst) {
            return m.get(src).get(dst);
        }
    }

}
