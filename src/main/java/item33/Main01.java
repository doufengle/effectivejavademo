package item33;

import java.util.EnumMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 用EnumMap代替序数索引
 */
public class Main01 {

    public static void main(String[] args) {

        // using ordinal() to index an array - don't do this!
        // 用序数做为数组索引 -- 不要这样
        Herb01[] garden = new Herb01[3];
        garden[0] = new Herb01("一年生", Herb01.Type.ANNUAL);
        garden[1] = new Herb01("二年生", Herb01.Type.BIENNIAL);
        garden[2] = new Herb01("三年生", Herb01.Type.PERENNIAL);
        // Indexed by Herb01.Type.ordinal()
        // 用序数做索引
        Set<Herb01>[] herbsByType = (Set<Herb01>[]) new Set[Herb01.Type.values().length];
        for (int i = 0; i < herbsByType.length; i++) {
            herbsByType[i] = new HashSet<Herb01>();
        }
        for (Herb01 h : garden)
            herbsByType[h.getType().ordinal()].add(h);
        // print the results
        for (int i = 0; i < herbsByType.length; i++) {
            System.out.printf("%s: %s%n", Herb01.Type.values()[i], herbsByType[i]);
        }

        Map<Herb01.Type, Set<Herb01>> herbsByType01 = new EnumMap<Herb01.Type, Set<Herb01>>(Herb01.Type.class);
        for (Herb01.Type t : Herb01.Type.values())
            herbsByType01.put(t, new HashSet<Herb01>());
        for (Herb01 h : garden)
            herbsByType01.get(h.getType()).add(h);
        System.out.println(herbsByType01);

        System.out.println(Phase01.Transition.from(Phase01.GAS, Phase01.LIQUID));


        // 最好不要用序数来索引数组，而要使用EnumMap。
    }

}
