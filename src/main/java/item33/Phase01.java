package item33;

// using ordinal() to index array of arrays - don't do this!
// 用序数做多维数组的索引 - 不要这样
public enum Phase01 {

    SOLID, LIQUID, GAS;

    public enum Transition {
        MELT, FREEZE, BOIL, CONDENSE, SUBLIME, DEPOSIT;
        // Rows indexed by src-ordinal, cols by dst-ordinal
        //
        private static final Transition[][] TRANSITIONS = {
                {null, MELT, SUBLIME},
                {FREEZE, null, BOIL},
                {DEPOSIT, CONDENSE, null}
        };

        // returns the phase transition from one phase to another
        public static Transition from(Phase01 src, Phase01 dst) {
            return TRANSITIONS[src.ordinal()][dst.ordinal()];
        }
    }

}
