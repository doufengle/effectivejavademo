package item19;

// constant utility class
// 常量工具类
public class PhysicalConstants02 {

    private PhysicalConstants02() {} //Prevents instantiation

    static final double AVOCADROS_NUMBER = 6.02214199e23;
    static final double BOLTZMANN_CONSTANT = 1.3806503e-23;
    static final double ELECTRON_MASS = 9.10938188e-31;

}
