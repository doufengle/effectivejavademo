package item19;

// constant interface antipattern - do not use!
// 常量接口模式是对接口的不良使用。
public interface PhysicalConstants01 {

    // Avogadro's number(1/mol)
    static final double AVOCADROS_NUMBER = 6.02214199e23;

    // Boltzmann constant (J/K)
    static final double BOLTZMANN_CONSTANT = 1.3806503e-23;

    // Mass of the electron (kg)
    static final double ELECTRON_MASS = 9.10938188e-31;

}
