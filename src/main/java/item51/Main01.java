package item51;

/**
 * 第51条：当心字符串连接的性能。
 */
public class Main01 {

    public static void main(String[] args) {

        // 为连接n个字符串而重复地使用字符串连接操作符，需要n的平方级的时间。
        long start = System.currentTimeMillis();
        statement(100000);
        System.out.println(System.currentTimeMillis() - start);
        start = System.currentTimeMillis();
        // 为了获得可以接受的性能，请使用StringBuilder替代String
        String result2 = statement2(100000);
        System.out.println(System.currentTimeMillis() - start);
    }

    public static String statement(int max) {
        String result = "";
        for (int i = 0; i < max; i++)
            result += "a" + i; // String concatenation 字符串连接
        return result;
    }

    public static String statement2(int max) {
        StringBuilder b = new StringBuilder(max * 2);
        for (int i = 0; i < max; i++){
            b.append("a");
            b.append(i);
        }
        return b.toString();
    }


}
