package item47;

import java.util.Random;

/**
 * 第47条：了解和使用类库
 */
public class Main01 {

    private static final Random rnd = new Random();

    public static void main(String[] args) {
        int n = 2 * (Integer.MAX_VALUE / 3);
        int low = 0;
        for (int i = 0; i < 1000000; i++)
            if (random(n) < n/2)
                low++;
        System.out.println(low);

        //直接使用类库产生位于0和某个上界之间的随机整数
        rnd.nextInt(n);
        // 通过使用标准类库，可以充分利用这些编写标准类库的专家的知识，以及在你之前的其他人的使用经验。
        // 不必浪费时间为那些与工作不太相关的问题提供特别的解决方案。
        // 就像大多数程序员一样，应该把时间花在应用程序上，而不是底层的细节上。
        // 它们的性能往往会随着时间的推移而不断提高，无需你做任何努力。

        // 在每个重要的发行版本中，都会有许多新的特性被加入到类库中，所以与这些新特性保持同步是值得的。
        // 每个程序员都应该熟悉java.lang、java.util，某种程序上还有java.io中的内容
        // java.util.concurrent包

    }

    // 产生位于0和某个上界之间的随机整数
    // common but deeply flawed!
    // 常见但是有严重的缺陷
    // 第一个缺点：如果n是一个比较小的2的乘方，经过一段相当短的周期之后，它产生的随机数序列将会重复。
    // 第二个缺点：如果n不是2的乘方，那么平均起来，有些数会比其他的数出现和旺铺为频繁。
    // 第三个缺点：在极少数情况下，它的失败是灾难性的，返回一个落在指定范围之外的数。
    static int random(int n) {
        return Math.abs(rnd.nextInt()) % n;
    }

}
