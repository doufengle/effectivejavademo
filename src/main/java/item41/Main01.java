package item41;

/**
 * 第41条：慎用重载
 */
public class Main01 {

    public static void main(String[] args) {

        // 要调用哪个重载(overloading)方法是在编译时做出决定的。
        // 对于重载方法(overloaded method)的选择是静态的，而对于被覆盖的方法(overridden method)的选择则是动态。
        // 应该避免胡乱地使用重载机制。
        // 安全而保守的策略是，永远不要导出两个具有相同参数数目的重载方法。

    }

}
