package item63;

/**
 * 第63条：在细节消息中包含能捕获失败的信息
 */
public class Main01 {

    public static void main(String[] args) {

        // 为了捕获失败，异常的细节信息应该包含所有“对该异常有贡献”的参数和域的值。

        /**
         * @see IndexOutOfBoundsException#IndexOutOfBoundsException(String)
         */

    }

}
