package item21;

// strategy interface
// 策略接口
public interface Comparator<T> {

    public int compare(T t1, T t2);

}
