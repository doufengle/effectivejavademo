package item21;

public class StringLengthComparator03 implements Comparator<String> {

    private StringLengthComparator03() {}

    public static final StringLengthComparator03 INSTANCE = new StringLengthComparator03();

    @Override
    public int compare(String s1, String s2) {
        return s1.length() - s2.length();
    }

}
