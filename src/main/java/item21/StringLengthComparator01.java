package item21;

public class StringLengthComparator01 {

    public int compare(String s1, String s2) {
        return s1.length() - s2.length();
    }

}
