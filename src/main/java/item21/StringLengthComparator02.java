package item21;

public class StringLengthComparator02 {

    private StringLengthComparator02() {}

    public static final  StringLengthComparator02 INSTANCE = new StringLengthComparator02();

    public int compare(String s1, String s2) {
        return s1.length() - s2.length();
    }

}
