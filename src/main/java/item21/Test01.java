package item21;

/**
 * 第21条：用函数对象表示策略
 */
public class Test01 {

    public static void main(String[] args) {
        /**
         * @see StringLengthComparator01
         * 函数对象
         */
        /**
         * @see StringLengthComparator02
         * 具体策略类
         *
         * @see Comparator
         * 策略接口
         *
         * @see StringLengthComparator03
         * 使用策略接口的实现类
         *
         * @see Host
         * 静态成员类
         */

    }

}
