package item21;

import java.io.Serializable;

public class Host {

    private static class StrLenCmp implements Comparator<String>, Serializable {
        @Override
        public int compare(String t1, String t2) {
            return t1.length() - t2.length();
        }
    }

    // returned comparator is serializable
    // 返回的比较器是可序列化的
    public static final Comparator<String> STRING_LENGTH_COMPARATOR = new StrLenCmp();

    //...
    // bulk of class omitted
    // 大部分省略

}
