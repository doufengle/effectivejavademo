package item43;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 第43条：返回零长度的数组或集合，而不是null
 */
public class Main01 {

    private final static List<Integer> cheesesInStock = new ArrayList<>();

    // the right way to return an array from a collection
    // 返回一个数组从一个集合的正确方法
    private static final Integer[] EMPTY_CHEESE_ARRAY = new Integer[0];

    public static void main(String[] args) {
        Integer[] integers = getCheeses();
        if (integers != null && Arrays.asList(integers).contains(1))
            System.out.println("Jolly good, just the thing.");

        integers = getCheeses01();
        if (Arrays.asList(integers).contains(1))
            System.out.println("Jolly good, just the thing.");

        // 返回类型为数组或集合的方法没理由返回null,而不是返回一零长度的数组或者集合。

    }

    public static Integer[] getCheeses() {
        if (cheesesInStock.size() == 0)
            return null;
        //...
        Integer[] integers = new Integer[cheesesInStock.size()];
        return cheesesInStock.toArray(integers);
    }

    public static Integer[] getCheeses01() {
        return cheesesInStock.toArray(EMPTY_CHEESE_ARRAY);
    }

    // the right way to return a copy of a collection
    // 返回一个集合的正确方法
    public List<Integer> getCheeseList() {
        if (cheesesInStock.isEmpty())
            return Collections.emptyList();
        else
            return new ArrayList<Integer>(cheesesInStock);
    }

}
