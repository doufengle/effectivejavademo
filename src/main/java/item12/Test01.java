package item12;

import java.util.*;

/**
 * 第12条：考虑实现Comparable接口
 */
public class Test01 {

    public static void main(String[] args) {
        // 实现了Comparable接口，就表明它的实例具有内在的排序关系。
        // 排序就这么简单
        String[] a = {"a","c","d","a","b"};
        Arrays.sort(a);
        for (String v : a) {
            System.out.println(v);
        }
        // 对存储在集合中的Comparable对象进行搜索、计算极限值以及自动维护也同样简单
        Set<String> s = new TreeSet<>();
        Collections.addAll(s, a);
        System.out.println(s);

        // 自反性、对称性、传递性

        // 如果不遵守(x.compareTo(y)==0) == (x.equals(y))时请明确予以说明
        // 注意：该类具有内在的排序功能，但是与equals不一致

        CaseInsensitiveString01 string01 = new CaseInsensitiveString01("b");
        CaseInsensitiveString01 string02 = new CaseInsensitiveString01("a");
        List<CaseInsensitiveString01> list = new ArrayList<>();
        list.add(string01);
        list.add(string02);
        Collections.sort(list);
        System.out.println(list);

        // 确信相关域不会为负值，或者最小和最大的可能域值之差等于int最大值
        // 不能保证就不要使用以下类的排序方法
        PhoneNumber02 number03 = new PhoneNumber02(77, 88, 99);
        PhoneNumber02 number02 = new PhoneNumber02(77, 88, 11);
        List<PhoneNumber02> phoneNumberList = new ArrayList<>();
        phoneNumberList.add(number02);
        phoneNumberList.add(number03);
        Collections.sort(phoneNumberList);
        System.out.println(phoneNumberList);


    }

}
