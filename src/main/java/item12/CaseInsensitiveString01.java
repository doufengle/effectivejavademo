package item12;

public final class CaseInsensitiveString01 implements Comparable<CaseInsensitiveString01> {

    private final String s;

    public CaseInsensitiveString01(String s) {
        if (s == null)
            throw new NullPointerException();
        this.s = s;
    }

    @Override
    public int compareTo(CaseInsensitiveString01 o) {
        return String.CASE_INSENSITIVE_ORDER.compare(s, o.s);
    }

    @Override
    public String toString() {
        return "CaseInsensitiveString01{" +
                "s='" + s + '\'' +
                '}';
    }
}
