package item12;

public class PhoneNumber01 implements Comparable<PhoneNumber01> {

    private final int areaCode;
    private final int prefix;
    private final int lineNumber;

    public PhoneNumber01(int areaCode, int prefix, int lineNumber) {
        this.areaCode = areaCode;
        this.prefix = prefix;
        this.lineNumber = lineNumber;
    }

    private static void rangeCheck(int arg, int max, String name) {
        if (arg < 0 || arg > max)
            throw new IllegalArgumentException(name + ": " + arg);
    }

    @Override
    public int compareTo(PhoneNumber01 o) {
        // compare area codes  比较区号
        if (areaCode < o.areaCode)
            return -1;
        if (areaCode > o.areaCode)
            return 1;
        // area codes are equal, compare prefixes  区号是相等的，比较前缀
        if (prefix < o.prefix)
            return -1;
        if (prefix > o.prefix)
            return 1;

        // area codes and prefixes are equal, compare line numbers 区号与前缀都是相等的，比较号码
        if (lineNumber < o.lineNumber)
            return -1;
        if (lineNumber > o.lineNumber)
            return 1;
        return 0; // all fields are equal  所有字段都是相等的
    }

}
