package item12;

public class PhoneNumber02 implements Comparable<PhoneNumber02> {

    private final int areaCode;
    private final int prefix;
    private final int lineNumber;

    public PhoneNumber02(int areaCode, int prefix, int lineNumber) {
        this.areaCode = areaCode;
        this.prefix = prefix;
        this.lineNumber = lineNumber;
    }

    private static void rangeCheck(int arg, int max, String name) {
        if (arg < 0 || arg > max)
            throw new IllegalArgumentException(name + ": " + arg);
    }

    @Override
    public int compareTo(PhoneNumber02 o) {
        // compare area codes  比较区号
        int areaCodeDiff = areaCode - o.areaCode;
        if (areaCodeDiff != 0)
            return areaCodeDiff;
        // area codes are equal, compare prefixes  区号是相等的，比较前缀
        int prefixDiff = prefix - o.prefix;
        if (prefixDiff != 0)
            return prefixDiff;
        // all fields are equal  所有字段都是相等的
        return lineNumber - o.lineNumber;
    }

    @Override
    public String toString() {
        return "PhoneNumber02{" +
                "areaCode=" + areaCode +
                ", prefix=" + prefix +
                ", lineNumber=" + lineNumber +
                '}';
    }
}
