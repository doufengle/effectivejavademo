package item02;

public class Test01 {

    public static void main(String[] args) {
        // 重叠构造器模式可行，但是当有许多参数的时候，客户端代码会很难编写，并且较难以阅读。
        NutritionFacts1 cocaCola01 = new NutritionFacts1(240, 8, 100, 0, 35,
                27);

        // JavaBean模式可行，创建实例很容易，容易阅读，
        // 但set过程中JavaBean可能处于不一致的状态,JavaBeans模式阻止了把类做成不可变的可能。
        NutritionFacts2 cocaCola02 = new NutritionFacts2();
        cocaCola02.setServingSize(240);
        cocaCola02.setCalories(8);
        cocaCola02.setCalories(100);
        cocaCola02.setSodium(35);
        cocaCola02.setCarbonhydrate(27);

        //

    }

}
