package item02;

// JavaBeans pattern - allows inconsistency, mandates mutability
public class NutritionFacts2 {

    // (ml) required
    private int servingSize;

    // (per container) required
    private int servings;

    //optional
    private int calories;

    // (g) optional
    private int fat;

    // (mg) optional
    private int sodium;

    // (g) optional
    private int carbonhydrate;

    public int getServingSize() {
        return servingSize;
    }

    public void setServingSize(int servingSize) {
        this.servingSize = servingSize;
    }

    public int getServings() {
        return servings;
    }

    public void setServings(int servings) {
        this.servings = servings;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public int getFat() {
        return fat;
    }

    public void setFat(int fat) {
        this.fat = fat;
    }

    public int getSodium() {
        return sodium;
    }

    public void setSodium(int sodium) {
        this.sodium = sodium;
    }

    public int getCarbonhydrate() {
        return carbonhydrate;
    }

    public void setCarbonhydrate(int carbonhydrate) {
        this.carbonhydrate = carbonhydrate;
    }

}
