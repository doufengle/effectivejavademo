package item02;

// Telescoping constructor pattern - does not scale well!
public class NutritionFacts3 {

    // (ml) required
    private final int servingSize;

    // (per container) required
    private final int servings;

    //optional
    private final int calories;

    // (g) optional
    private final int fat;

    // (mg) optional
    private final int sodium;

    // (g) optional
    private final int carbonhydrate;

    private NutritionFacts3(Builder builder) {
        this.servingSize = builder.servingSize;
        this.servings = builder.servings;
        this.calories = builder.calories;
        this.fat = builder.fat;
        this.sodium = builder.sodium;
        this.carbonhydrate = builder.carbonhydrate;
    }

    public static class Builder {
        //required parameters
        private final int servingSize;
        private final int servings;

        //optional parameters - initialized to default values
        private int calories = 0;
        private int fat = 0;
        private int sodium = 0;
        private int carbonhydrate = 0;

        public Builder(int servingSize, int servings) {
            this.servingSize = servingSize;
            this.servings = servings;
        }

        public Builder calories(int val) {
            calories = val;
            return this;
        }

        public Builder fat(int val) {
            fat = val;
            return this;
        }

        public Builder sodium(int val) {
            sodium = val;
            return this;
        }

        public Builder carbonhydrate(int val) {
            carbonhydrate = val;
            return this;
        }

        public NutritionFacts3 build() {
            return new NutritionFacts3(this);
        }
    }

}
