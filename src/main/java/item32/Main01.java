package item32;

import java.util.EnumSet;

/**
 * 第32条：用EnumSet代替位域
 */
public class Main01 {

    public static void main(String[] args) {

        /**
         * @see Text01
         * 位常量类
         */

        Text01 text01 = new Text01();
        text01.applyStyles(Text01.STYLE_BOLD | Text01.STYLE_ITAIC);

        /**
         * @see Text02
         * @see java.util.EnumSet
         */
        Text02 text02 = new Text02();
        text02.applyStyles(EnumSet.of(Text02.Style.BOLD, Text02.Style.ITALIC));

        // 正是因为枚举类型要用在集合(Set)中，所以没有理由用位域来表示它。
    }

}
