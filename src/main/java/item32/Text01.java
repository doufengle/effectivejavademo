package item32;

// bit field enumeration constants - obsolete!
// 位域枚举常量 - 已过时
public class Text01 {

    public static final int STYLE_BOLD = 1 << 0; //1
    public static final int STYLE_ITAIC = 1 << 1; //2
    public static final int STYLE_UNDERLINE = 1 << 2; //4
    public static final int STYLE_STRIKETHROUGH = 1 << 3; //8

    // parameter is bitwise or of zero or more style_ constants
    // 参数是位或0或更多的style_的常量
    public void applyStyles(int styles) {
        // ...
    }

}
