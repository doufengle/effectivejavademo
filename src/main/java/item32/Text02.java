package item32;

import java.util.Set;

// enumSet - a modern replacement for bit fields
// enumSet - 位域的代替品
public class Text02 {

    public enum Style{BOLD, ITALIC, UNDERLINE, STRIKETHROUGH}

    // any set could be passed in, but EnumSet is clearly best
    // 可传入任何集合，但是EnumSet显然是最好的
    public void applyStyles(Set<Style> styles) {
        // ...
    }

}
