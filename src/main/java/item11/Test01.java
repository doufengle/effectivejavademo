package item11;

/**
 * 第11条 谨慎地覆盖clone
 */
public class Test01 {

    public static void main(String[] args) {

        // 如果你覆盖了非final类中的clone方法，则应该返回一个通过调用super.clone而得到的时候。
        // 对于实现了Cloneable的类，我们总是期望它也提供一个功能适当的公有clone方法
        // 你必须确保它不会伤害到原始的对象，并确保正确地创建被克隆对象中的约束条件
        // clone架构与引用可变对象的final域的正常用法是不相兼容的
        // 最好提供某些其他的途径来代替对象拷贝，或者干脆不提供这样的功能。
        // 提供一个拷贝构造器或拷贝工厂

    }

}
