package item16;

import java.util.*;

/**
 * 第16条：复合优先于继承
 * 实现继承，当一个类扩展另一个类的时候
 */
public class Test01 {

    public static void main(String[] args) {

        // 与方法调用不同的是，继承打破了封装性。

        InstrumentedHashSet01<String> s = new InstrumentedHashSet01<String>();
        List list = Arrays.asList("snap", "crackle", "pop");
        s.addAll(list);
        System.out.println(s.getAddCount());

        InstrumentedSet<String> s1 = new InstrumentedSet<String>(new TreeSet<String>());
        s1.addAll(list);
        System.out.println(s1.getAddCount());

    }

}
