package item10;

/**
 * 第10条 始终要覆盖toString
 */
public class Test01 {

    public static void main(String[] args) {
        // 提供好的toString实现可以使类用起来更加舒适
        // 无论你是否决定指定格式，都应该在文档中明确地表明你的意图
        // 无论是否指定格式，都为toString返回值中包含的所有信息，提供一种编程式的访问途径
    }

}
