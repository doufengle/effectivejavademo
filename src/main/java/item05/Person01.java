package item05;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class Person01 {

    private final Date birthDate;

    public Person01(Date birthDate) {
        this.birthDate = birthDate;
    }

    // Other fields, methods, and constructor omitted
    // don't do this!
    public boolean isBabyBoomer() {
        // unnecessary allocation of expensive object
        // 不必要的昂贵对象分配
        Calendar gmtCal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        gmtCal.set(1946, Calendar.JANUARY, 1, 0, 0, 0);
        Date boomStart = gmtCal.getTime();
        gmtCal.set(1965, Calendar.JANUARY, 1, 0, 0, 0);
        Date boomEnd = gmtCal.getTime();
        return birthDate.compareTo(boomStart) >= 0 &&
                birthDate.compareTo(boomEnd) < 0;
    }

}
