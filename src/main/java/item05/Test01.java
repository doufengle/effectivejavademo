package item05;

import java.util.Date;

/**
 * 避免创建不必要的对象
 */
public class Test01 {

    public static void main(String[] args) {
        // don't do this
        String s1 = new String("stringette");

        // 改进后
        String s2 = "stringette";

        // 改造前
        Person01 p1 = new Person01(new Date());
        System.out.println(p1.isBabyBoomer());

        // 改造后
        Person02 p2 = new Person02(new Date());
        System.out.println(p2.isBabyBoomer());

        cumulative();
    }

    public static void cumulative() {

        //要优先使用基本类型而不是装箱基本类型，要当心无意识的自动装箱

        // hideously slow program! can you spot the object creation?
        long start = System.currentTimeMillis();
        //这里声明成装箱基本类型，后面循环会一直创建小对象
        Long sum = 0L;
        //long sum = 0L;
        for (long i = 0; i < Integer.MAX_VALUE; i++) {
            sum += i;
        }
        System.out.println(sum);
        System.out.println(System.currentTimeMillis() - start);
    }

}
