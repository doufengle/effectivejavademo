package item23;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Class01 {

    // now a raw collection type - don't do this!
    // 现在是一个原始类型的集合——请不要这样做
    /**
     * My stamp collection. Contains only stamp instances.
     * 我的原始类型集合。仅包含原始初始化。
     */
    private static final Collection stamps = new ArrayList();

    public static void main(String[] args) {

        // Erroneous insertion of String into integer collection
        // 错误的插入字符串类型到数值集合中
        stamps.add(new String("aaaa"));
        stamps.add(new Integer(1));

        // Now a raw iterator type - don't do this
        // 现在是一个原始类型的迭代器——请不要这样做
        for (Iterator i = stamps.iterator(); i.hasNext();) {
            Integer in = (Integer) i.next(); // Throws ClassCastException
            //...
            // do something with the integer
            // 用数值类型做一些事
        }

    }

}
