package item23;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Class02 {

    // Parameterized collection type - typesafe
    // 参数化集合类型——类型安全
    private static final Collection<Integer> integers = new ArrayList<Integer>();

    public static void main(String[] args) {
        integers.add(new Integer(1));
        // 编译错误
        // integers.add(new String("bbbbb"));

        // for-each loop over a parameterized collection - typesafe
        // for-each 循环参数化集合 ——类型安全
        for (Integer a : integers) {
            // ...
            // do something with the integer
            // 做一些事情用数值类型
        }

        // for loop with parameterized iterator declaration - typesafe
        // for 循环参数化迭代器-类型安全
        for (Iterator<Integer> i = integers.iterator(); i.hasNext();) {
            Integer in = i.next(); // no cast necessary 不需要强转
            // ...
            // do something with the integer
            // 做一些事用数值类型
        }

    }
}
