package item23;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Class03 {

    // uses raw type(List) - fails at runtime!
    // 使用原始类型(列表) - 在运行时错误
    public static void main(String[] args) {
        List<String> strings = new ArrayList<String>();
        unsafeAdd(strings, new Integer(42));
        String s = strings.get(0);  // compiler-generated cast 编译器生成的转换
        // list<String> 不能转 list<Object>,他们都是原始类型list的参数化类型
        // unsafeAdd01(strings, new Integer(12));
    }

    private static void unsafeAdd(List list, Object o) {
        list.add(o);
    }

    private static void unsafeAdd01(List<Object> list, Object o) {
        list.add(o);
    }

    // use of raw type for unknown element type - don't do this!
    // 对未知元素类型使用原始类型 - 请不要这样做
    static int numElementsInCommon(Set s1, Set s2) {
        int result = 0;
        for (Object o1 : s1)
            if (s2.contains(o1))
                result++;
        return result;
    }

    // unbounded wildcard type - typesafe and flexible
    // 无限制的通配符类型 - 类型安全而且灵活
    static int numElementsInCommon01(Set<?> s1, Set<?> s2) {
        int result = 0;
        for (Object o1 : s1)
            if (s2.contains(o1))
                result++;
        return result;
    }

    static void test(Set<?> s1) {
        // 不能将任何元素(除了null之外)放到collection<?>中
        // s1.add("ver");
    }

}
