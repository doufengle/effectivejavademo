package item23;

import com.sun.org.apache.xerces.internal.xs.StringList;

import java.util.ArrayList;
import java.util.List;

/**
 * 第23条：请不要在新代码中使用原生态类型
 */
public class Test01 {

    public static void main(String[] args) {

        // 原生态类型(raw type)
        List list = new ArrayList();

        // 泛型
        List<String> stringList = new ArrayList<>();

        // 如何使用原生态类型，就失掉了泛型在安全性和表述性方面的所有优势。
        // 如果使用像List这样的原生态类型，就会失掉类型安全性，但是如果使用像List<Object>这样的参数化类型，则不会

        // 在类文字(class literal)中必须使用原生态类型
        // List.class String[].class int.class 合法， List<String.class> 和List<?>.class则不合法

        // 利用泛型来使用instanceof操作符的首选方法
        // legitimate use of raw type - instanceof operator
        // 合法使用原始类型- instanceof 操作
        if (stringList instanceof List) {
            List<?> l = stringList;

        }
    }

}
