package item13;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Example02 {

    private static final Character[] PRIVATE_VALUES = {'a','b','c'};

    // 把公有数组变成私有的，并增加一个公有的不可变列表
    public static final List<Character> VALUES = Collections.unmodifiableList(Arrays.asList(PRIVATE_VALUES));

}
