package item13;

public class Example03 {

    private static final Character[] PRIVATE_VALUES = {'a','b','c'};

    // 可以使数组变成私有的，并添加一个公有方法，它返回私有数组的一个备份
    public static final Character[] values() {
        return PRIVATE_VALUES.clone();
    }

}
