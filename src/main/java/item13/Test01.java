package item13;

import java.util.Arrays;

/**
 * 使类和成员的可访问性最小化
 */
public class Test01 {

    public static void main(String[] args) {

        // 信息隐藏或封装
        // 尽可能地使每个类或者成员不被外界访问
        // 实例域决不能是公有的
        // 包含公有可变域的类并不是线程安全的
        // 类具有公有的静态final数组域，或者返回这种域的访问方法，这几乎总是错误的
        System.out.println(Arrays.asList(Example01.VALUES));
        // 一个线程
        Example01.VALUES[0] = 'b';
        // 另一个线程
        Example01.VALUES[0] = 'c';
        // 第一个线程使用
        System.out.println(Example01.VALUES[0]);

        System.out.println(Example02.VALUES.get(0));
        // 这里会异常，返回的是不可修改的
        Example02.VALUES.remove(0);
        System.out.println(Example02.VALUES.get(0));


        System.out.println(Example03.values()[0]);
        Example03.values()[0] = 'c';
        System.out.println(Example03.values()[0]);

    }

}
