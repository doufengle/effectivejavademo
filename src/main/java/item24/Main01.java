package item24;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 第24条：消除非受检警告
 */
public class Main01 {

    public static void main(String[] args) {
        List<String> exaltation = new ArrayList<>();
        exaltation.add("afafa");

        // 要尽可能的消除每一个非受检警告。
        // 如果无法消除警告，同时可以证明引起警告的代码是类型安全的，
        // (只有在这种情况下)才可以用一个@suppressWarnings("unchecked")注解来禁止这条警告。
        // suppress Warnings 抑制警告
        /**
         * @see Arrays#copyOf(int[], int)
         */

    }

}
