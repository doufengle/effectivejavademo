package item57;

/**
 * 第57条：只针对异常的情况才使用异常
 */
public class Main01 {

    public static void main(String[] args) {

        Mountain[] range = new Mountain[2];
        Mountain m1 = new Mountain();
        Mountain m2 = new Mountain();
        range[1] = m1;
        range[2] = m2;
//        try {
//            int i = 0;
//            while (true) range[i++].climb();
//        } catch (ArrayIndexOutOfBoundsException e) {
//
//        }
        // 这个循环企图访问数组边界之外的第一个数组元素时，用抛出、捕获、忽略异常的手段来达到终止无限循环的目的。

        // 因为异常机制的设计初衷是用于不正常的情形，所以很少会有JVM实现试图对它们进行优化，使得与显式的测试一样快速。
        // 把代码放在try-catch块中反而阻止了现代JVM实现本来可能要执行的某些特定优化。
        // 对数组进行遍历的标准模式并不会导致冗余的检查。有些现代的JVM实现会将它们优化掉。

        // 顾名思义，异常应该只用于异常的情况下;它们永远不应该用于正常的控制流。

        // 设计良好的API不应该强迫它的客户端为了正常的控制流而使用异常。


    }

}
