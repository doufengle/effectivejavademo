package item61;

/**
 * 第61条：抛出与抽象相对应的异常
 */
public class Main01 {

    public static void main(String[] args) {

        // 更高层的实现应该捕获低层的异常，同时抛出可以按照高层抽象进行解释的异常。

        // Exception Translation
        //try {
        // use lower-level abstraction to do our bidding
        // } catch(LowerLevelException e) {
        // throw new HigherLevelException();
        // }

        /**
         * @see java.util.AbstractSequentialList#get(int)
         * 异常转译
         */

        // Exception Chaining 异常链
//        try {
//            // use lower-level abstraction to do our bidding
//        } catch (LowerLevelException cause) {
//            throw new HigherLevelException(cause);
//        }

        // 尽管异常转译与不加选择地从低层传递异常的做法相比有所改进，但是它也不能被滥用。



    }

}

class HigherLevelException extends Exception {
    HigherLevelException(Throwable cause) {
        super(cause);
    }
}
