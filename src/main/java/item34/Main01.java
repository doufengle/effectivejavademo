package item34;

import java.util.Arrays;
import java.util.Collection;

/**
 * 第34条：用接口模拟申缩的枚举
 */
public class Main01 {

    public static void main(String[] args) {
        double x = Double.parseDouble("2.0");
        double y = Double.parseDouble("3.0");
        test01(ExtendedOperation.class, x, y);
        test02(Arrays.asList(ExtendedOperation.values()), x, y);

        // 虽然无法编写可扩展的枚举类型，却可以通过编写接口以及实现该接口的基础枚举类型，对它进行模拟。
    }

    private static <T extends Enum<T> & Operation01> void test01(Class<T> opSet, double x, double y) {
        for (Operation01 op : opSet.getEnumConstants())
            System.out.printf("%f %s %f = %f%n", x, op, y, op.apply(x, y));
    }

    private static void test02(Collection<? extends Operation01> opSet, double x, double y) {
        for (Operation01 op : opSet)
            System.out.printf("%f %s %f = %f%n", x, op, y, op.apply(x, y));
    }

}
