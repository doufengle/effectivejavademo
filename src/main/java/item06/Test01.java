package item06;

/**
 * 消除过期的对象引用
 */
public class Test01 {

    public static void main(String[] args) {

        Stack01 stack01 = new Stack01();
        stack01.push(1);
        stack01.pop();

        Stack02 stack02 = new Stack02();
        stack02.push(1);
        stack02.pop();

    }

}
