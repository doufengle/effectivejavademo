package item06;

import java.util.Arrays;
import java.util.EmptyStackException;

// Can you spot the "memory leak"?
// 你能找出“内存泄漏”吗?
public class Stack02 {

    private Object[] elements;
    private int size = 0;
    private static final int DEFAULT_INITIAL_CAPACITY = 16;

    public Stack02() {
        elements = new Object[DEFAULT_INITIAL_CAPACITY];
    }

    public void push(Object e) {
        ensureCapacity();
        elements[size++] = e;
    }

    public Object pop() {
        if (size == 0)
            throw new EmptyStackException();
        Object result = elements[--size];
        elements[size] = null; //eliminate obsolete reference 消除过时的引用
        return elements[--size];
    }

    /**
     * Ensure space for at least one more element, roughly
     * doubling the capacity each time the array needs to grow.
     *
     * 粗略地说，确保至少多一个元素的空间
     * 当数组需要增长时，将容量增加一倍。
     */
    private void ensureCapacity() {
        if (elements.length == size)
            elements = Arrays.copyOf(elements, 2 * size + 1);
    }

}
