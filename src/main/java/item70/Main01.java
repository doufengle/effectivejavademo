package item70;

import java.util.Map;

public class Main01 {

    public static void main(String[] args) {
        // 因为在一个方法声明中出现synchronized修饰符，这是个实现细节，并不是导出的API的一部分。

        // 一个类为了可被多个线程安全地使用，必须在文档中清楚地说明它所支持的线程安全性级别。

        // 常见的线程安全级别：
        // 1.不可变的(immutable) 这个类的实例是不变的。所有不需要外部的同步。这样的例子包括String、Long和BigInteger.

        // 2.无条件的线程安全(unconditionally thread-safe) 这个类的实例是可变的，但是这个类有着足够的内部同步，所以，它的
        // 实例可以被并发使用，无需任何外部同步。其例子包括Random和ConcurrentHashMap

        // 3.有条件的线程安全(conditionally thread-safe) 除了有些方法为进行安全的并发使用而需要外部同步之外，这种线程安全级别与
        // 无条件的线程安全相同。这样的例子包括Collections.synchronized包装返回的集合，它们的迭代器(iterator)要求外部同步。

        // 4.非线程安全(not thread-safe) 这个类的实例是可变的。为了并发地使用它们，客户必须利用自己选择外部同步包围每个方法调用
        // (或者调用序列)。这样的例子包括通用的集合实现，例如ArrayList和HashMap.

        // 5.线程对立的(threa-hostile) 这个类不能安全地被多个线程并发使用，即使所有的方法调用都被外部同步包围。线程对立的根源通常在于，
        // 没有同步地修改静态数据。没有人会有意编写一个线程对立的类;这种类是因为没有考虑到并发性而产生的后果。幸运的是，在java平台类库中，
        // 线程对立的类或者方法非常少。System.runFinalizersOnExit方法是线程对立的，但已经被废除了。

        /**
         * @see java.util.Collections#synchronizedMap(Map)
         * 说明
         * 当遍历任何被返回Map的集合视图时，用户必须手工对它们进行同步
         */


    }

}
