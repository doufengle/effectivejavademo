package item78;

import java.io.Serializable;
import java.util.Date;

// Serialization proxy for period class
public class SerializationProxy implements Serializable {

    private final Date start;
    private final Date end;

    public SerializationProxy(Period p) {
        this.start = p.start();
        this.end = p.end();
    }

    // readResolve method for Period.SerializationProxy
    private Object readResolve() {
        return new Period(start, end); //Uses public constructor
    }

    private static final long serialVersionUID = 234098243823485285L; // any number will do
}
