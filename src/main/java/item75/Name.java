package item75;

import java.io.Serializable;

// good candidate for default serializable form
public class Name implements Serializable {

    /**
     * last name. must to non-null.
     * @serial
     */
    private final String lastName;

    /**
     * First name. must be non-null.
     * @serial
     */
    private final String firstName;

    /**
     * middle name, or null if there is none.
     * @serial
     */
    private final String middleName;

    public Name(String lastName, String firstName, String middleName) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
    }

}
