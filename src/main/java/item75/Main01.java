package item75;

/**
 * 第75条：考虑使用自定义的序列化形式
 */
public class Main01 {

    public static void main(String[] args) {

        // 如果没有先认真考虑默认的序列化形式是否合适，则不要贸然接受。

        // 如果一个对象的物理表示法等同于它的逻辑内容，可能就适合于使用默认的序列化形式。
        /**
         * @see Name
         */

        // 即使你确定了默认的序列化形式是合适的，通常还必须提供一个readObject方法以保证约束关系和安全性。

        // 当一个对象的物理表示法与它的逻辑数据内容有实质性的区别时，使用默认序列化形式会有以下4个缺点：
        // 1.它使这个类的导出API永远地束缚在该类的内部表示法上。
        // 2.它会消耗过多的空间。
        // 3.它会消耗过多的时间。
        // 4.它会引起栈溢出。


        // 如果所有的实例域都是瞬时的，从技术角度而言，不调用defaultWriteObject和defaultReadObject也是允许的，但是不推荐这样做。

        // 在决定将一个域做成非transient的之前，请一定要确信它的值将是该对象逻辑状态的一部分。

        // 如果在读取整个对象状态的任何其他方法上强制任何同步，则也必须在对象序列化上强制这种同步。

        // 不管你选择了哪种序列化形式，都要为自己编写的每个可序列化的类声明一个显式的序列版本UID。

    }

}
