package item45;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 第45条：将局部变量的作用域最小化
 */
public class Main01 {

    public static void main(String[] args) {

        // perferred idiom for iterating over a collection
        // 用于迭代集合的特定习语
        List<Integer> list = new ArrayList<>();
        for (Integer i : list) {
            // dosomething
        }

        // no for-each loop or generics before release 1.5
        // 在1.5之前没有foreach和泛型时首选做法
        for (Iterator i = list.iterator(); i.hasNext();) {
            //dosomething
            System.out.println(i.next());
        }

        // Compile-time error - cannot find symbol i
        // 编译错误，不能找到符合i
//        for (Iterator i2 = list.iterator(); i.hasNext();) {
//            //dosomething
//            System.out.println(i2.next());
//        }

        Iterator i = list.iterator();
        while (i.hasNext()) {
            System.out.println(i.next());
        }

        Iterator i2 = list.iterator();
        while (i.hasNext()) {  // bug 用的i而不是i2
            System.out.println(i2.next());
        }

        for (int a = 0, n = expensiveComputation(); a < n; a++) {
            //dosomething
        }

        // 要使局部变量的作用域最小化，最有力的方法就是在第一次使用它的地方声明。
        // 几乎每个局部变量的声明都应该包含一个初始化表达式。
        // 如果在循环终止之后不再需要循环变量的内容，for循环就优先于while循环。
        // 使方法小而集中

    }

    static int expensiveComputation() {
        return 20;
    }

}
