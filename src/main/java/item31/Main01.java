package item31;

public class Main01 {

    public static void main(String[] args) {
        /**
         * @see Ensemble01
         * 不要用枚举的序数(ordinal)
         * 不要这样做
         */

        // 永远不要根据枚举的序数导出与它关联的值，而是要将它保存在一个实例域中。

        /**
         * @see Ensemble02
         * 要这样做
         */
    }

}
