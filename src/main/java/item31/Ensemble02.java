package item31;

public enum Ensemble02 {

    SOLO(1),
    DUET(2),
    TRIO(3),
    QUARTET(4),
    QUINTET(5),
    SEXTET(6),
    SEPTET(7),
    OCTET(8),
    MONET(9),
    DECTET(10);

    private final int numberOfMusicians;

    Ensemble02(int size) {
        this.numberOfMusicians = size;
    }

    public int numberOfMusicians() {
        return numberOfMusicians;
    }

}
