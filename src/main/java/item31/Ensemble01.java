package item31;

// abuse of ordinal to derive an associated value - don't do this
// 滥用序数导出相关的值 - 不要这样做
public enum Ensemble01 {

    SOLO, DUET, TRIO, QUARTET, QUINTET, SEXTET, SEPTET, OCTET, MONET, DECTET;

    public int numberOfMusicians() {
        return ordinal() + 1;
    }
}
