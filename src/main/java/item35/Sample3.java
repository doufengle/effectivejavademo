package item35;

import java.util.ArrayList;
import java.util.List;

public class Sample3 {

    @ExceptionTest02({IndexOutOfBoundsException.class, NullPointerException.class})
    public static void doublyBad() {
        List<String> list = new ArrayList<>();
        list.addAll(5, null);
    }
}
