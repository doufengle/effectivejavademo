package item03;

public class Test01 {

    public static void main(String[] args) {
        //可以通过反射机制调用私有构造器
        //jvm几乎都能够将静态工厂方法的调用内联化
        Elvis1 elvis1 = Elvis1.INSTANCE;

        //可以通过反射机制调用私有构造器
        //提供灵活性：在不改变其API的前提下，我们可以改变该类是否应该为singleton的想法
        Elvis2 elvis2 = Elvis2.getInstance();

        //更加简洁，无偿提供了序列化机制，
        //绝对防止多次实例化，即使是在面对复杂的序列化或者反射攻击的时候。
        Elvis3 elvis3 = Elvis3.INSTANCE;
    }

}
