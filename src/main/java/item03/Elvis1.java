package item03;

// Singleton with public final field
public class Elvis1 {

    public static final Elvis1 INSTANCE = new Elvis1();

    private Elvis1() {
        //初始化
    }

    public void leaveTheBuilding() {
        //离开构建
    }

}
