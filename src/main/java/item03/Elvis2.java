package item03;

import java.io.Serializable;

// Singleton with static factory
public class Elvis2 implements Serializable {

    private static final Elvis2 INSTANCE = new Elvis2();

    private Elvis2() {
        //初始化
    }

    public static Elvis2 getInstance() {
        return INSTANCE;
    }

    public void leaveTheBuilding() {
        //离开构建
    }

    //readResolve method to preserve singleton property
    private Object readResolve() {
        // return the one true Elvis and let the garbage collecton
        // take care of the Elvis impersonator
        return INSTANCE;
    }

}
