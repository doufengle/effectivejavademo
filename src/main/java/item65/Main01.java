package item65;

/**
 * 第65条：不要忽略异常
 */
public class Main01 {

    public static void main(String[] args) {

        // empty catch block ignores exception - highly suspect!
        // 空捕获块忽略异常
        try {

        } catch (Exception e) {
        }

        // 空的catch块会使异常达不到应有的目的。
        // catch块也应该包含一条说明，解释为什么可以忽略这个异常。
        

    }

}
