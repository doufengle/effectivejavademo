package item18;

// 歌唱家
public interface Singer {

    void sing(String song);

}
