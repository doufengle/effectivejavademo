package item18;

// 既是歌唱家也是作曲家
public interface SingerSongwriter {

    void strum();
    void actSensitive();

}
