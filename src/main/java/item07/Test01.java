package item07;

/**
 * 避免使用终结方法
 */
public class Test01 {

    //终结方法守卫者
    private final Object finalizerGuardian = new Object() {
        @Override
        protected void finalize() throws Throwable {
            //finalize outer object
        }
    };

    public static void main(String[] args) {
        // 终结方法通常是不可预测的，也是危险的，一般情况下是不必要的。
        // 不应该依赖终结方法来更新重要的持久状态
        // 使用终结方法有一个非常严重的(Severe)性能损失
        // 有时提供一个显式的终止方法是比较好，显式的终止方法通常与try-finally结构结合起来使用，以确保及时终止
        // 一般终结方法充当“安全网(safety net),如果终结方法发现资源还未被终止，则应该在日志中记录一条警告”
        /**
         * @see java.io.FileInputStream
         * @see java.io.FileOutputStream
         * @see java.util.Timer
         * @see java.sql.Connection
         */
        //终止非关键的本地资源(本地对等体 native peer)

        //为了避免忘记调用父类的终结方法,出现了终结方法守卫者

    }

    /**
     * 终结方法模板
     * @throws Throwable
     *  manual finalizer chaining
     */
    @Override
    protected void finalize() throws Throwable {
        try {
            //finalize subclass state
        } finally {
            super.finalize();
        }
    }
}
