package item36;

public class Bigram02 {

    private final char first;
    private final char second;

    public Bigram02(char first, char second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Bigram02))
            return false;
        Bigram02 b = (Bigram02) o;
        return b.first == first && b.second == second;
    }

    public int hashCode() {
        return 31 * first + second;
    }

}
