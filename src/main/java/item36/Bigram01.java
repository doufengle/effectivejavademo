package item36;

public class Bigram01 {

    private final char first;
    private final char second;

    public Bigram01(char first, char second) {
        this.first = first;
        this.second = second;
    }

    public boolean equals(Bigram01 b) {
        return b.first == first && b.second == second;
    }

    public int hashCode() {
        return 31 * first + second;
    }

}
