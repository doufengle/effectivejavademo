package item36;

import java.util.HashSet;
import java.util.Set;

/**
 * 第36条:坚持使用override注解
 */
public class Main01 {

    public static void main(String[] args) {
        Set<Bigram01> s = new HashSet<Bigram01>();
        for (int i = 0; i < 10; i++)
            for (char ch = 'a'; ch <= 'z'; ch++)
                s.add(new Bigram01(ch, ch));
        System.out.println(s.size());

        Set<Bigram02> s1 = new HashSet<Bigram02>();
        for (int i = 0; i < 10; i++)
            for (char ch = 'a'; ch <= 'z'; ch++)
                s1.add(new Bigram02(ch, ch));
        System.out.println(s1.size());

        // 想要覆盖超类声明的每个方法声明中使用Override注解。

    }

}
