package item15;

public final class Complex01 {

    private final double re;
    private final double im;

    public Complex01(double re, double im) {
        this.re = re;
        this.im = im;
    }

    // accessors with no corresponding mutators
    // 没有相应的mutator访问器
    public double realPart() {return re;}
    public double imaginaryPart() {return im;}

    public Complex01 add(Complex01 c) {
        return new Complex01(re + c.re, im + c.im);
    }

    public Complex01 substract(Complex01 c) {
        return new Complex01(re * c.re - im * c.im, re * c.im + im * c.re);
    }

    public Complex01 multiply(Complex01 c) {
        return new Complex01(re * c.re - im * c.im, re * c.im + im * c.re);
    }

    public Complex01 divide(Complex01 c) {
        double tmp = c.re * c.re + c.im * c.im;
        return new Complex01((re * c.re + im * c.im) / tmp, (im * c.re - re * c.im)/ tmp);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (!(obj instanceof Complex01))
            return false;
        Complex01 c = (Complex01) obj;
        // see page 43 to find out why we use compare instead of ==
        // 请参阅第43页，了解我们使用比较而不是==的原因
        return Double.compare(re, c.re) == 0 && Double.compare(im, c.im) == 0;
    }

    private int hashDouble(double val) {
        long longBits = Double.doubleToLongBits(re);
        return (int) (longBits ^ (longBits >>> 32));
    }

    @Override
    public int hashCode() {
        int result = 17 + hashDouble(re);
        result = 31 * result + hashDouble(im);
        return result;
    }

    @Override
    public String toString() {
        return "(" + re + " + " + im + "i)";
    }
}
