package item15;

import java.math.BigInteger;

/**
 * 第15条：使可变性最小化
 */
public class Test01 {

    // 频繁用到的值，为它们提供公有的静态final常量。
    public static final Complex01 ZERO = new Complex01(0, 0);
    public static final Complex01 ONE = new Complex01(1, 0);
    public static final Complex01 I = new Complex01(0, 1);

    public static void main(String[] args) {
        // 使类成为不可变的五条规则
        // 1.不要提供任何会修改对象状态的方法
        // 2.保证类不会被扩展
        // 3.使所有的域都是final的
        // 4.使所有的域都成为私有的
        // 5.确保对于任何可变组件的互斥访问

        // 不可变对象本质上是线程安全的，它们不要求同步。
        // 不可变对象可以被自由地共享。

        // 不仅可以共享不可变对象，甚至也可以共享它们的内部信息。
        // 不可变对象为其他对象提供了大量的构件。
        // 不可变类真正唯一的缺点是，对于每个不同的值都需要一个单独的对象。

        /**
         * @see java.math.BigDecimal
         * @see java.math.BigInteger
         * @see Integer
         * @see Long
         * @see Short
         * @see Byte
         */

        // 除非有很好的理由要让类成为可变的类，否则就应该是不可变的
        // 如果类不能被做成是不可变的，仍然应该尽可能地限制它的可变性
        // 除非有令人信服的理由要使域变成是非final的，否则要使每个域都是final的。


    }

    public static BigInteger safeInstance(BigInteger val) {
        if (val.getClass() != BigInteger.class)
            return new BigInteger(val.toByteArray());
        return val;
    }

}
